# frozen_string_literal: true

class Paydesk
  ADDITIONAL_TASK = false

  def initialize(bills_hash, amount)
    @bills_hash = bills_hash
    @amount     = amount
  end

  def call
    # write your bill composition code here!
  end
end
