# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) do
    {
      '100': 2,
      '50': 1,
      '20': 2,
      '10': 4,
      '5': 1,
      '1': 2
    }
  end

  it 'returns expected hash' do
    expect(described_class.new(bills_hash, 40).call).to eq('20': 2)
  end
end
